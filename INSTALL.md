# Setup and Installation

Install the requirements. 
- These are located in `./requirements.txt`. 
- They can be installed with `pip3 install -r requirements.txt` or `conda create –-name trees_env –-file requirements.txt`.

Or to create a docker image with the all the dependencies pre-installed, use the following repo: https://github.com/ankitkariryaa/keras-rasterio
