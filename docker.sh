# Rasterio-keras container
docker run --runtime=nvidia --name usingPublicKnowledge -p 8888:8888 -v /media:/media -v $PWD:/notebooks -d ankitkariryaa/keras-rasterio:2.0.1-gpu-py3

# GDAL docker image
docker run --rm -v /home:/home -ti osgeo/gdal bash
# gdal_translate -of GTiff dop20rgb_32567_5934_1_hh_2016.jpg dop20rgb_32567_5934_1_hh_2016.tif
# 
