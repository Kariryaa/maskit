
# MaskIt: Masking for efficiently utilization of incomplete public datasets for training deep learning models  
This repository contains the model (UNet) and other essential tools for detecting trees in a masked input. The code was written by Ankit Kariryaa
Kariryaa AT uni-bremen.de in 2018. Please contact him if you have any questions.

## Setup and Installation
See [INSTALL](./INSTALL.md).

## Structure

The code is structured in Jupyter notebooks available in the noteooks folder. Each notebook contains a considerable part of the pipeline. The notebookssupported with core libraries available in the notebooks/core directory. Follow these steps for training a model from scratch and analyzing the aerial images.


### Step 1: Data preparation
A part of the aerial images should be annotated with the trees, the areas that are annotated should be separately marked and stored as shapefiles.
Initialize the variables in the CleanAndSeparate.ipynb notebook to extact these areas, the road mask and the respective trees as image files. 

### Step 2: Model training
Train the machine learning model with the extracted images through the 2-UNetTraining.ipynb notebook. 
